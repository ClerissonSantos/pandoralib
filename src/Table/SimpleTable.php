<?php

namespace ClerissonSantos\Pandoralib\Table;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class SimpleTable
{
    protected $headers;

    protected $source;

    protected $data;

    protected $checkBox = false;

    protected $buttons = [];

    protected $paginate = false;

    protected $paramsRequest = [];

    public function headers(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function source($source)
    {
        $this->source = $source;
        return $this;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getCheckBox()
    {
        return $this->checkBox;
    }

    public function getPaginate()
    {
        return $this->paginate;
    }

    public function setCheckBox($value)
    {
        $this->checkBox = $value;
        return $this;
    }

    public function setParamsRequest(array $params)
    {
        $this->paramsRequest = $params;
        return $this;
    }

    public function getParamsRequest()
    {
        return $this->paramsRequest;
    }

    public function getButtons()
    {
        return $this->buttons;
    }

    private function _prepareTable()
    {
        if ($this->source instanceof LengthAwarePaginator) {
            $this->data     = $this->source->items();
            $this->paginate = true;
            return;
        }

        if ($this->source instanceof Collection) {
            $this->data = $this->source;
            return;
        }
    }

    public function __toString()
    {
        $this->_prepareTable();

        return view('pandoralib::tables.table')->with([
            'table' => $this
        ])->render();
    }

    public function addButton($tag, array $options, $icon = '', $value = '')
    {
        $this->buttons[] = [
            'tag'        => $tag,
            'attributes' => $options,
            'icon'       => $icon,
            'value'      => $value
        ];
        return $this;
    }
}