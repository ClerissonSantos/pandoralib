<?php

namespace ClerissonSantos\PandoraLib\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ClerissonSantos\PandoraLib\Db\DataBaseConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SimpleModel extends Model
{
    use DataBaseConfig;
    use SoftDeletes;

    protected $fillable;

    public function __construct(array $attributes = [])
    {
        $columns = $this->getDbColumns($this->table);

        foreach ($columns as $column) {
            $this->fillable[] = $column->COLUMN_NAME;
        }

        parent::__construct($attributes);
    }

    public function salvar($dados, $sessionFlush = true)
    {
        if ($dados instanceof Request) {
            $dados = $dados->all();
        }

        $response  = $this->updateOrCreate(['id' => isset($dados['id']) ? $dados['id'] : '' ], $dados);
        self::gerarLog($response,$dados['id'] > 0 ? 'Registro atualizado' : 'Registro adicionado');
        $tableName = ucfirst($this->table);
        $request   = request();

        if (!isset($dados['redirect']) && $request->ajax() || !$sessionFlush) return $response;

        if ($response) {
            if (isset($dados['id'])) {
                $this->_adicionarMensagemSessao([
                    'type' => 'success',
                    'msg'  => "{$tableName} atualizado com sucesso!"
                ]);
            } else {
                $this->_adicionarMensagemSessao([
                    'type' => 'success',
                    'msg'  => "{$tableName} cadastrado com sucesso!"
                ]);
            }
        } else {
            $this->_adicionarMensagemSessao([
                'type' => 'error',
                'msg'  => 'Algo errado aconteceu! :('
            ]);
        }
        return $response;
    }

    public function deletar($dados)
    {
        if ($dados instanceof Request) {
            $dados = $dados->all();
        }

        if (is_array($dados['id'])) {
            $erro = [];

            foreach ($dados['id'] as $item) {
                $registro = $this->find($item);
                $response = $registro->delete();
                self::gerarLog($registro, 'Registro excluído');

                if (!$response) $erro[] = $item;
            }

        } else {
            $dados    = $this->find($dados['id']);
            $response = $dados->delete();
            self::gerarLog($dados, 'Registro excluído');

            if (!$response) $erro[] = $item;
        }

        $tableName = ucfirst($this->table);
        $request   = request();

        if (!isset($dados['redirect']) && $request->ajax()) return $response;

        if (count($erro) > 0) {
            $ids = implode("|", $erro);
            $this->_adicionarMensagemSessao([
                'type' => 'warning',
                'msg'  => "Não conseguimos excluir os ids {$ids}"
            ]);
        } else {
            $this->_adicionarMensagemSessao([
                'type' => 'success',
                'msg'  => "$tableName excluido com sucesso"
            ]);
        }
    }


    private function _adicionarMensagemSessao($mensagemNova)
    {
        $mensagens = session()->get('mensagens') ?: [];

        array_push($mensagens, $mensagemNova);

        session()->flash('mensagens', $mensagens);
    }

    public function gerarLog($dados, $acao = null)
    {
        return DB::table('log')->insert([
            'usuario_nome'   => Auth::user()->name,
            'alteracao'      => json_encode($dados),
            'tabela'         => $this->table,
            'chave'          => isset($dados['id']) ? $dados['id'] : null,
            'acao'           => $acao,
            'data_alteracao' => Carbon::now()->format('Y-m-d h:i:s'),
        ]);
    }
}