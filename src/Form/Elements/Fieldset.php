<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Fieldset extends SimpleFormItem
{
    public $view = 'fieldset';
}