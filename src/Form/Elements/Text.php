<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Text extends SimpleFormItem
{
    public $view = 'generic';
}