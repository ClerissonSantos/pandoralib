<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Autocomplete extends SimpleFormItem
{
    public $view = 'autocomplete';
    
    public function __construct($type, $name, $label, $options, $view, &$simpleForm)
    {
        parent::__construct($type, $name, $label, $options);

        $simpleForm->add('hidden', "{$name}_id", '', [], $view = 'generic');
    }
}