<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Number extends SimpleFormItem
{
    public $view = 'generic';
}