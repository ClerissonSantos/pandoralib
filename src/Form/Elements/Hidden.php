<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Hidden extends SimpleFormItem
{
    public $view = 'generic';
    public $noLabel = true;
}