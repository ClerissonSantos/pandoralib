<?php
/**
 * Created by PhpStorm.
 * User: clerisson
 * Date: 23/09/2018
 * Time: 21:18
 */

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Date extends SimpleFormItem
{
    public $view = 'generic';
}