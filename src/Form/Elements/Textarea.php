<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Textarea extends SimpleFormItem
{
    public $view = 'textarea';
}