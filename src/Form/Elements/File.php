<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class File extends SimpleFormItem
{
    public $view = 'generic';
}