<?php

namespace ClerissonSantos\PandoraLib\Form\Elements;

use ClerissonSantos\PandoraLib\Form\SimpleFormItem;

class Color extends SimpleFormItem
{
    public $view = 'generic';
}