<?php

namespace ClerissonSantos\PandoraLib\Form;

class SimpleFormItem
{
    public $id;
    public $name;
    public $value;
    public $label;
    public $class       = 'form-control';
    public $noLabel     = false;
    public $view;
    public $type;
    public $data        = [];
    public $options     = [];
    public $columns     = 3;
    public $required    = '';
    public $disabled    = '';
    public $readonly    = '';
    public $maxlength   = '';
    public $accept      = '';
    public $multiSelect = '';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param int $columns
     */
    public function setColumns(int $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return bool
     */
    public function isNoLabel(): bool
    {
        return $this->noLabel;
    }

    /**
     * @param bool $noLabel
     */
    public function setNoLabel(bool $noLabel)
    {
        $this->noLabel = $noLabel;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     */
    public function setView(string $view)
    {
        $this->view = $view;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function setRequired($value)
    {
        $this->required = $value;
    }

    public function setMaxlength($value)
    {
        $this->maxlength = "maxlength=$value";
    }

    public function setDisabled()
    {
        $this->disabled = 'disabled';
    }

    public function setReadonly()
    {
        $this->readonly = 'readonly';
    }

    public function setData($value)
    {
        $this->data = $value;
    }

    public function setAccept($value)
    {
        $this->accept = "accept=$value";
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options): void
    {
        $this->options = $options;
    }

    public function setMultiselect()
    {
        $this->multiSelect = "multiple";
    }

    public function getMultiselect()
    {
        return $this->multiSelect;
    }

    public function __construct($type, $name, $label, $options)
    {
        $this->type  = $type;
        $this->name  = $name;
        $this->label = $label;

        foreach ($options as $method => $value) {
            $methodName = 'set' . ucfirst($method);

            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }
    }

    public function __toString()
    {
        return view('pandoralib::forms.form-item-wrapper')->with([
            'type'        => $this->type,
            'name'        => $this->name,
            'id'          => $this->id,
            'class'       => $this->class,
            'label'       => $this->label,
            'col'         => $this->columns,
            'value'       => $this->value,
            'noLabel'     => $this->noLabel,
            'extends'     => "form-item-{$this->view}",
            'options'     => $this->options,
            'required'    => $this->required,
            'disabled'    => $this->disabled,
            'readonly'    => $this->readonly,
            'data'        => $this->data,
            'maxlength'   => $this->maxlength,
            'accept'      => $this->accept,
            'multiSelect' => $this->multiSelect,
        ])->render();
    }
}