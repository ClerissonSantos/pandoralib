<?php

namespace ClerissonSantos\PandoraLib\Form;

use Illuminate\Database\Eloquent\Model;
use ClerissonSantos\PandoraLib\Db\DataBaseConfig;
use Illuminate\Http\Request;

class SimpleForm
{
    use DataBaseConfig;

    public $formName;

    protected $model;

    protected $method;

    protected $action;

    protected $name;

    protected $enctype;

    protected $id;

    public $fields = [];

    protected $buttons = [];

    protected $removeFields = [];

    /**
     * @return mixed
     */
    private function getAction()
    {
        return $this->action ? " action=$this->action" : "";
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    private function getName()
    {
        return $this->name ? " name=$this->name" : "";
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    private function getId()
    {
        return $this->id ? $this->id : last(explode("\\", get_called_class()));
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    private function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function setButton(array $button)
    {
        $this->buttons[] = $button;
        return $this;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function setEnctype($value)
    {
        $this->enctype = "enctype=$value";
        return $this;
    }

    public function getEnctype()
    {
        return $this->enctype;
    }

    private function _setupElements()
    {
        if ($table = $this->model->getTable()) {

            $columns = $this->getDbColumns($table);

            $this->add('hidden', '_token', '', ['value' => csrf_token()]);

            foreach ($columns as $column) {

                if ($column->COLUMN_NAME == 'created_at' || $column->COLUMN_NAME == 'updated_at'
                    || $column->COLUMN_NAME == 'deleted_at') {
                    continue;
                }

                if ($column->COLUMN_NAME == 'id') {
                    $this->add('hidden', $column->COLUMN_NAME, '', ['columns' => 0]);
                    continue;
                }

                if ($column->IS_NULLABLE == 'NO') {
                    $options['required'] = 'required';
                }

                $label            = $this->_prepareLabel($column);
                $options['value'] = $column->COLUMN_DEFAULT;

                switch ($column->DATA_TYPE) {
                    case 'text' :
                    case 'varchar' :
                    case 'char' :
                    case 'longtext' :
                        $this->add('text', $column->COLUMN_NAME, $label, $options);
                        break;
                    case 'int' :
                    case 'integer' :
                    case 'smallint' :
                        $this->add('number', $column->COLUMN_NAME, $label, $options);
                        break;
                    case 'decimal' :
                        $this->add('decimal', $column->COLUMN_NAME, $label, $options);
                        break;
                    case 'date' :
                    case 'datetime' :
                        $this->add('date', $column->COLUMN_NAME, $label, $options);
                        break;
                    case 'enum' :
                        $options['options'] = $this->_prepareEnum($column);
                        $this->add('enum', $column->COLUMN_NAME, $label, $options);
                        break;
                }

                unset($options);
            }

            $this->buttons['submit'] = [
                'type'    => 'submit',
                'class'   => 'btn btn-success ',
                'value'   => 'Salvar',
                'id'      => 'salvar',
                'icon'    => 'fa fa-check',
                'options' => []
            ];
        }

        $this->init();
    }

    public function startNewForm(Model $model)
    {
        $this->model = $model;

        $this->_setupElements();

        return $this;
    }

    public static function source(Model $model)
    {
        $class        = new Static($model);
        $class->model = $model;

        $class->_setupElements();

        return $class;
    }

    public function __toString()
    {
        return view('pandoralib::forms.form')
            ->with([
                'method'  => $this->getMethod(),
                'action'  => $this->getAction(),
                'name'    => $this->getName(),
                'id'      => $this->getId(),
                'enctype' => $this->getEnctype(),
                'buttons' => $this->buttons,
                'fields'  => $this->fields
            ])
            ->render();
    }

    public function add(string $type, string $name, string $label = '', array $options = [], $view = 'generic')
    {
        $nameSpace           = "ClerissonSantos\PandoraLib\Form\Elements\\" . ucfirst($type);
        $this->fields[$name] = new $nameSpace($type, $name, $label, $options, $view, $this);
        return $this;
    }

    private function _prepareLabel($column)
    {
        $label = $column->COLUMN_COMMENT;

        if ($label == '') {
            $name  = str_replace('_', ' ', $column->COLUMN_NAME);
            $label = ucwords($name);
        }

        if ($column->IS_NULLABLE == "NO") {
            $label = "*$label";
        }

        return $label;
    }

    public function removeElements($fields)
    {
        if (!is_array($fields)) $fields = [$fields];

        foreach ($fields as $field) {
            if (isset($field, $this->fields)) unset($this->fields[$field]);
        }

        $this->removeFields = $fields;
    }

    public function populate(array $data)
    {
        foreach ($data as $field => $value) {
            if (isset($this->fields[$field])) {
                $this->fields[$field]->setValue($value);
            }
        }

        return $this;
    }

    private function _prepareEnum($column)
    {
        $enum       = str_replace([')', 'enum(', "'"], '', $column->COLUMN_TYPE);
        $options    = explode(',', $enum);
        $newOptions = [];

        foreach ($options as $option) {
            $newOptions[$option] = $option;
        }

        return $newOptions;
    }

    public function setSizeFields(array $fieldsSize)
    {
        foreach ($fieldsSize as $field => $size) {
            if (array_key_exists($field, $this->fields)) {
                $this->fields[$field]->columns = $size;
            }
        }
    }

    public function elementsFilter(array $elements = [])
    {
        $this->formFilter();

        if (!count($elements) > 0) return $this;

        foreach ($this->fields as $element) {
            if (!in_array($element->name, $elements)) {
                unset($this->fields[$element->name]);
            } else {
                $this->fields[$element->name]->required = '';
                $this->fields[$element->name]->label    = str_replace('*', '', $this->fields[$element->name]->label);
            }
        }

        $this->add('hidden', '_token','',[
            'value' => csrf_token()
        ]);

        $this->buttons['submit']['value'] = 'Pesquisar';
        $this->buttons['submit']['id']    = 'pesquisar-proposta';
        $this->buttons['submit']['icon']  = 'fa fa-search';
        $this->setMethod('get');
        $this->setAction('');
        $this->removeButton('index');

        return $this;
    }

    public function removeButton($name)
    {
        if (array_key_exists($name, $this->buttons)) {
            unset($this->buttons[$name]);
        }
    }

    protected function setOrder(array $orderFields)
    {
        $newFields = [];
        foreach ($orderFields as $field) {
            $newFields[$field] = $this->fields[$field];
        }
        $this->fields = $newFields;
    }

    protected static function makeOptions(array $data, $selecione = true)
    {
        if ($selecione) {
            $data[0] = 'Selecione';
        }

        asort($data);
        return $data;
    }

    public function formFilter()
    {
        $request = request();

        if (strlen($this->getId()) > 0) {
            $sessionName = $this->getId();
        } else {
            $sessionName = last(explode('\\', get_called_class()));
        }

        if ($request->session()->exists($sessionName)) {
            $fields = $request->session()->get($sessionName);
            $request->merge(array_merge($fields, $request->all()));
            $request->session()->put($sessionName, $request->all());
        } else {
            $request->session()->put($sessionName, $request->all());
        }

        $this->populate($request->all());
    }
}