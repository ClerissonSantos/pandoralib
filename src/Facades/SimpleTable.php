<?php namespace ClerissonSantos\PandoraLib\Facades;

use Illuminate\Support\Facades\Facade;

class SimpleTable extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'SimpleTable'; }

}