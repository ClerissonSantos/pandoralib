@foreach($table->getButtons() as $button)
    <?php $attributes = ''?>
    @foreach($button['attributes'] as $attrKey => $attrValue)
        <?php
            $attrTemp    = vsprintf($attrValue, $data->toArray());
            $attributes .= " $attrKey='{$attrTemp}'"
        ?>
    @endforeach

    <{!! $button['tag'] !!} {!! $attributes !!} >

    @if(!empty($button['icon']))
        <i class="{{ $button['icon'] }}"></i>
    @endif

    @if(!empty($button['value']))
        {{ $button['value'] }}
    @endif

    </{!! $button['tag'] !!}>
@endforeach
