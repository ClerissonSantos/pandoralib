<thead>
    <tr>
        @if($table->getCheckBox())
            <th>
                <input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;">
            </th>
        @endif

        @if(count($table->getButtons()) > 0)
            <th></th>
        @endif

        @foreach($table->getHeaders() as $column => $label)
            <th> {{ $label }} </th>
        @endforeach
    </tr>
</thead>
