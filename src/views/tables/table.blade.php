<div class="table-responsive">
    <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
        @include('pandoralib::tables.header')

        @include('pandoralib::tables.body')
    </table>
</div>
@if($table->getPaginate())
    {{ $table->getSource()->appends($table->getParamsRequest())->links() }}
@endif