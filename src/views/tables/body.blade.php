<tbody>
    @forelse($table->getData() as $data)
    <tr>
    @if($table->getCheckBox())
        <td style="padding-top: 11px;">
            <input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;" id="{{ $data->id }}" />
        </td>
    @endif
    @if(count($table->getButtons()) > 0)
        <td>
         @include('pandoralib::tables.buttons', $data)
        </td>
    @endif
        @foreach($table->getHeaders() as $hKey => $hValue)
            <td style="padding-top: 1em;">
                <?php
                    if (strpos($hKey, '.')) {
                        $header = explode('.', $hKey);
                        echo $data->{$header[0]}->{$header[1]};
                    } else {
                        echo $data->$hKey;
                    }
                ?>
            </td>
        @endforeach
    </tr>
    @empty
        <tr>
            <td></td>
            <td colspan="12">Nenhum item localizado</td>
        </tr>
    @endforelse
</tbody>
