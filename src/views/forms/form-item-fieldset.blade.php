<fieldset name="{{ $name }}" style="margin: 20px 0px 0px 0px;">
    <legend class="text-info">{{ $label }}</legend>
</fieldset>