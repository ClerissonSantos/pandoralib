<input
    id="{{ isset($id) ? "$id-autocomplete" : "$name" }}"
    type="text"
    name="{{ $name }}"
    class="{{ $class . ' autocomplete' }}"
    value="{{ $value }}"
    {{ $required }}
    {{ $disabled }}
    {{ $readonly }}

    @if (!empty($data))
        @foreach ($data as $dataChave => $dataValor)
            <?php echo " data-{$dataChave}='{$dataValor}' "?>
        @endforeach
    @endIf
/>