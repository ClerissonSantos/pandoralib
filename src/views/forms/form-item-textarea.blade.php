<textarea class="form-control m-b {{ $class }}" name="{{ $name }}" id="{{ isset($id) ? $id : $name }}" rows="18">
{{ $value }}
</textarea>
