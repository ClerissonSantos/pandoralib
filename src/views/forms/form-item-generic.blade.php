<input
    id="{{ isset($id) ? $id : $name }}"
    type="{{ $type }}"
    name="{{ $name }}"
    class="{{ $class }}"
    value="{{ $value }}"
    {{ $required }}
    {{ $disabled }}
    {{ $readonly }}
    {{ $maxlength }}
    {!! $accept !!}

    @if (!empty($data))
        @foreach ($data as $dataChave => $dataValor)
            <?php echo " data-{$dataChave}='{$dataValor}' "?>
        @endforeach
    @endIf
/>