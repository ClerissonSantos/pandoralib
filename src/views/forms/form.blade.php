<form class="form-horizontal"
      method="{{ $method }}"
      id="{{ $id }}"
      {{ $action }}  {{ $name }} {!! $enctype !!} autocomplete="off">

    <?php $columns = 0;?>
    @foreach ($fields as $field)

        @if ($field->getType() == "hidden")
            {!! $field !!}
            @continue
        @endif()

        @if ($columns == 0)
            <div class="row">
        @endif

        @if ($columns + $field->getColumns() > 12)
            </div>
            <div class="row">
            {!! $field !!}
            <?php $columns = $field->getColumns();?>
            @continue
        @endif

        {!! $field !!}

        <?php $columns += $field->getColumns();?>

        @if ($columns == 12)
            </div>
            <?php $columns = 0;?>
        @endif

    @endforeach

    @if($columns > 0 && $columns < 12)
        </div>
    @endif

    @if(count($buttons) > 0)
        <div style="clear: both;">
        @foreach($buttons as $button)
            @include('pandoralib::forms.form-item-button', $button)
        @endforeach
        </div>
    @endif

</form>