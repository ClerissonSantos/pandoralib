<input
    id="{{ isset($id) ? $id : $name }}"
    type="text"
    name="{{ $name }}"
    class="{{ $class }}"
    value="{{ $value }}"
    {{ $required }}
    {{ $disabled }}
    {{ $readonly }}/>