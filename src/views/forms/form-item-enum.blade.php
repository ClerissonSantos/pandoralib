<select {!! $multiSelect !!} class="{{ $class }}" {{ $disabled }} name="{{ $name }}" id="{{ isset($id) ? $id : $name }}" {{ $required }} {{ $readonly }}>
    @foreach($options as $key => $name)
        <option value="{{ $key }}" <?php echo $key == $value || is_array($value) && in_array($key, $value) ? 'selected' : ''?>>{{ $name }}</option>
    @endforeach
</select>