
    <?php $attributes = ''; ?>

    @foreach($options as $attrKey => $attrValue)
        <?php $attributes .= " $attrKey={$attrValue}" ?>
    @endforeach

    <{{ isset($tag) ? $tag : 'button'  }} style="margin-top: 1.9em;"
        href="{{ isset($href) ? $href : '#' }}"
        id="{{ $id }}"
        name="{{ $name }}"
        class="{{ $class }}"
        type="{{ $type }}"
        {{ $attributes }}>
        <i class="{{ $icon }}"></i>
        {{ $value }}
    </{{ isset($tag) ? $tag : 'button' }}>