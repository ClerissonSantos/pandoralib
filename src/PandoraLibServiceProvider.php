<?php

namespace ClerissonSantos\PandoraLib;

use Illuminate\Support\ServiceProvider;

class PandoraLibServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__. '/views', 'pandoralib');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SimpleTable','ClerissonSantos\PandoraLib\Table\SimpleTable');
    }
}
