<?php

namespace ClerissonSantos\PandoraLib\Controller;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SimpleController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}