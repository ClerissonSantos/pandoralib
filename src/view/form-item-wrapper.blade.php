<div class="col-md-{{ $col }}" {{ $type == 'hidden' ? 'hidden' : '' }}>
    @if(!$noLabel)
        <label class="control-label" style="padding-top: 8px;">{{ $label }}: </label>
    @endif
    @include('form-item-generic')
</div>