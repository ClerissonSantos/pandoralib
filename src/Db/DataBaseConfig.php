<?php

namespace ClerissonSantos\Pandoralib\Db;

use Illuminate\Support\Facades\DB;

trait DataBaseConfig
{
    public function getDbColumns($tableName)
    {
        $db = env('DB_DATABASE');
        return DB::select(
            "SELECT * FROM information_schema.columns WHERE table_schema = '{$db}' AND table_name = '{$tableName}'"
        );
    }
}